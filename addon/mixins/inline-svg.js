/*! @anamo/ember-inline-svg v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-inline-svg*/

import Mixin from '@ember/object/mixin'

export default Mixin.create({
	tagName: 'svg',
	attributeBindings: 'xmlns aria-hidden role data-icon viewBox width:width height:height'.w(),

	xmlns: 'http://www.w3.org/2000/svg',
	'aria-hidden': 'true',

	get 'data-icon'() {
		return this.toString().split('@component:', 2).lastObject.split('::').firstObject // <@anamo/market-dashboard-web-app@component:svg/info-circle-duotone::ember576>
	},

	role: 'img',

	width: 24,
	height: 24,

	actions: {}
})
